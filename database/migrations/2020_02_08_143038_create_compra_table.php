<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compra', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idProducto')->unsigned();
            $table->integer('idCliente')->unsigned();
            $table->integer('idEmpresa')->unsigned();
            $table->timestamps();

            $table->foreign('idProducto')->references('id')->on('producto');
            $table->foreign('idCliente')->references('id')->on('cliente');
            $table->foreign('idEmpresa')->references('id')->on('empresa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compra');
    }
}
