@extends('app')

@section('content')
<div class="row" id="crud">
    <div class="col-xs-12">
        <h1 class="page-header">
            Productos Cuero Colombia
        </h1>
    </div>
    <div class="col-sm-7">
        <a class="btn btn-primary pull-right" href="#" data-toggle="modal" data-target="#create">
            Nueva Cuero
        </a>
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Nombre
                    </th>
                    <th colspan="2">
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="empresa in empresas">
                    <td width="10px">
                        @{{ empresa.id }}
                    </td>
                    <td>
                        @{{ empresa.nombre }}
                    </td>
                    <td width="10px">
                        <a class="btn btn-warning btn-sm" href="#" v-on:click.prevent="editKeep(empresa)">
                            Editar
                        </a>
                    </td>
                    <td width="10px">
                        <a class="btn btn-danger btn-sm" href="#" v-on:click.prevent="deleteKeep(empresa)">
                            Eliminar
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>

        <nav>
        	<ul class="pagination">
        		<li v-if="pagination.current_page > 1">
        			<a href="#" @click.prevent="changePage(pagination.current_page - 1)">
        				<span>Atras</span>
        			</a>
        		</li>
        		<li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
        			<a href="#" @click.prevent="changePage(page)">
        				@{{ page }}
        			</a>
        		</li>
        		<li v-if="pagination.current_page < pagination.last_page">
        			<a  href="#" @click.prevent="changePage(pagination.current_page + 1)">
        				<span>Siguiente</span>
        			</a>
        		</li>
        	</ul>
        </nav>

        @include('create')
        @include('edit')
    </div>
    <div class="col-sm-5">
        <pre>
    		@{{ $data }}
    	</pre>
    </div>
</div>
@endsection
